---
layout: page
title: About
permalink: /about/EternalBlueFlame
---

I started Software engineering at the age of 13, I always enjoyed it as a hobby but it wasn't until adulthood that I decided to persue it as a career choice.
To a great extent I can thank Traincraft and it's wonderful community for that, Traincraft inspired me to take software engineering more seriously, and the community encouraged me to always keep improving and advancing.
